<?php

/**
 * @file
 * Enables modules and site configuration for the dd8 profile.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\contact\Entity\ContactForm;

/**
 * Implements hook_form_FORM_ID_alter() for install_settings_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function dd8_profile_form_install_settings_form_alter(&$form, FormStateInterface $form_state) {

	$db_name = getenv('DB_NAME') ?: '';
	$db_user = getenv('DB_USER') ?: '';
	$db_pwd = getenv('DB_PASSWORD') ?: '';
	$db_host = getenv('DB_HOST') ?: 'localhost';
	$db_port = getenv('DB_PORT') ?: '3306';
	$db_driver = getenv('DB_DRIVER') ?: 'mysql';


	// prefill database information for docker4drupal default
	$form['settings']['mysql']['docker4drupal-information'] = [
		'#markup' => "<strong style='color:red;font-weight:bold'>Default values reflect docker4drupal stack default settings. Feel free to adapt to your needs !</strong>",
		'#weight' => -10
 	];


	// change password field type to show default value
	$form['settings']['mysql']['password']['#type'] = "textfield";

	$form['settings']['mysql']['database']['#default_value'] = $db_name;
	$form['settings']['mysql']['username']['#default_value'] = $db_user;
	$form['settings']['mysql']['password']['#default_value'] = $db_pwd;


	$form['settings']['mysql']['advanced_options']['host']['#default_value'] = $db_host;
	$form['settings']['mysql']['advanced_options']['port']['#default_value'] = $db_port;
	$form['settings']['mysql']['advanced_options']['#open'] = TRUE;
}


/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function dd8_profile_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
    $form['site_information']['site_name']['#attributes']['placeholder'] = t('DRUPAL 8 - GRUMPY DISTRIBUTION');
    $form['site_information']['site_name']['#default_value'] = 'site built with GRUMPY distribution';
    $form['site_information']['site_mail']['#default_value'] = 'dev@grumpydev.com';

    $form['admin_account']['account']['name']['#default_value'] = 'admin';
    $form['admin_account']['account']['mail']['#default_value'] = 'dev@grumpydev.com';

    $form['regional_settings']['site_default_country']['#default_value'] = 'CH';
    $form['regional_settings']['date_default_timezone']['#default_value'] = 'Europe/Paris';

    $form['update_notifications']['update_status_module']['#default_value'] = TRUE;
    $form['update_notifications']['enable_update_status_emails']['#default_value'] = FALSE;

    $form['#submit'][] = 'dd8_form_install_configure_submit';
}

/**
 * Submission handler to sync the contact.form.feedback recipient.
 */
function dd8_form_install_configure_submit($form, FormStateInterface $form_state) {
    //$site_mail = $form_state->getValue('site_mail');
    //ContactForm::load('feedback')->setRecipients([$site_mail])->trustData()->save();
}
